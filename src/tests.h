#ifndef MEMORY_ALLOCATOR_TESTS_H
#define MEMORY_ALLOCATOR_TESTS_H

#include "mem.h"
#include "mem_internals.h"

    void successful_memory_allocation_test();
    void freeing_one_block_from_several_allocated_test();
    void freeing_two_blocks_from_several_allocated_test();
    void memory_ended_and_expanded_test();
    void memory_ended_and_expanded_elsewhere_test();

#endif
