#include "tests.h"
#include <stdlib.h>

#define ERROR_FOOTER "------TEST FAILED------"
#define SUCCESS_FOOTER "------TEST SUCCESSFUL------"

void print_error(const char* msg) {
    fprintf(stderr, "%s", msg);
}

void print_info(const char* msg) {
    puts(msg);
}

static struct block_header* block_get_header(void *contents) {
  return (struct block_header *)(((uint8_t *)contents) - offsetof(struct block_header, contents));
}

void heap_free(void* heap) {
    munmap(heap, size_from_capacity((block_capacity){.bytes = 10000}).bytes);
}

void successful_memory_allocation_test(){
    void* heap = heap_init(10000);

    if (!heap) {
        print_error("memory allocation error\n");
        print_info(ERROR_FOOTER);
        return;
    }
    else{
        print_info("memory allocated\n");
    }
    debug_heap(stdout, heap);

    void *block1 = _malloc(1000);
    if (block1){
        print_info("block#1 allocated\n");
    }
    else {
        print_error("block#1 allocation error\n");
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);

    _free(block1);
    print_info("<block1> freed\n");
    debug_heap(stdout, heap);

    heap_free(heap);
    print_info(SUCCESS_FOOTER);
}

void freeing_one_block_from_several_allocated_test(){
    void *heap = heap_init(1707);
    if (!heap) {
        print_error("memory allocation error\n");
        print_info(ERROR_FOOTER);
        return;
    }
    else{
        print_info("memory allocated\n");
    }
    debug_heap(stdout, heap);

    void *block1 = _malloc(170);
    if (block1){
        print_info("block#1 allocated\n");
    }
    else {
        print_error("block#1 allocation error\n");
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);
    
    void *block2 = _malloc(600);
    if (block2){
        print_info("block#2 allocated\n");
    }
    else {
        print_error("block#2 allocation error\n");
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);

    void *block3 = _malloc(700);
    if (block3)
        print_info("block#3 allocated\n");
    else {
        print_error("block#3 allocation error\n");
        _free(block1);
        _free(block2);
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);

    _free(block2);
    print_info("block#2 freed\n");
    debug_heap(stdout, heap);

    _free(block1);
    print_info("block#1 freed\n");
    _free(block3);
    print_info("block#3 freed\n");
    heap_free(heap);
    print_info(SUCCESS_FOOTER);
}

void freeing_two_blocks_from_several_allocated_test(){
    void *heap = heap_init(1707);
    if (!heap) {
        print_error("memory allocation error\n");
        print_info(ERROR_FOOTER);
        return;
    }
    else{
        print_info("memory allocated\n");
    }
    debug_heap(stdout, heap);

    void *block1 = _malloc(170);
    if (block1){
        print_info("block#1 allocated\n");
    }
    else {
        print_error("block#1 allocation error\n");
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);
    
    void *block2 = _malloc(600);
    if (block2){
        print_info("block#2 allocated\n");
    }
    else {
        print_error("block#2 allocation error\n");
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);

    void *block3 = _malloc(700);
    if (block3)
        print_info("block#3 allocated\n");
    else {
        print_error("block#3 allocation error\n");
        _free(block1);
        _free(block2);
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);
    
    void *block4 = _malloc(200);
    if (block4){
        print_info("block#4 allocated\n");
    }
    else {
        print_error("block#4 allocation error\n");
        _free(block1);
        _free(block2);
        _free(block3);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);

    _free(block2);
    print_info("block#2 freed\n");
    debug_heap(stdout, heap);

    _free(block4);
    print_info("block#4 freed\n");
    debug_heap(stdout, heap);

    _free(block1);
    print_info("block#1 freed\n");
    _free(block3);
    print_info("block#3 freed\n");
    debug_heap(stdout, heap);

    heap_free(heap);
    print_info(SUCCESS_FOOTER);   
}

void memory_ended_and_expanded_test(){
    void *heap = heap_init(1000);
    if (!heap) {
        print_error("memory allocation error\n");
        print_info(ERROR_FOOTER);
        return;
    }
    else{
        print_info("memory allocated\n");
    }
    debug_heap(stdout, heap);
    void *block = _malloc(5000);
    if (block)
        print_info("block allocated\n");
    else {
        print_error("block allocation error\n");
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);
    _free(block);
    print_info("block freed\n");
    debug_heap(stdout, heap);
    heap_free(heap);
    print_info(SUCCESS_FOOTER);
}

void memory_ended_and_expanded_elsewhere_test(){
    void *heap = heap_init(4);
    if (!heap) {
        print_error("memory allocation error\n");
        print_info(ERROR_FOOTER);
        return;
    }
    else{
        print_info("memory allocated\n");
    }
    debug_heap(stdout, heap);

    void *block1 = _malloc(1024);
    if (block1){
        print_info("block#1 allocated\n");
    }
    else {
        print_error("block#1 allocation error\n");
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);

    struct block_header *header = block_get_header(block1);
    (void)mmap(header -> contents + header -> capacity.bytes, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_FIXED, -1, 0);

    void *block2 = _malloc(2048);
    if (block1){
        print_info("block#2 allocated\n");
    }
    else {
        print_error("block#2 allocation error\n");
        heap_free(heap);
        print_info(ERROR_FOOTER);
        return;
    }
    debug_heap(stdout, heap);

    _free(block1);
    print_info("block#1 freed\n");
    _free(block2);
    print_info("block#2 freed\n");
    munmap(header -> next, REGION_MIN_SIZE);
    munmap(heap, REGION_MIN_SIZE);
    print_info(SUCCESS_FOOTER);
}


