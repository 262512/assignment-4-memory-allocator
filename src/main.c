#include "tests.h"

int main(){
    puts("TEST 1: Normal successful memory allocation");
    successful_memory_allocation_test();
    puts("TEST 2: Freeing one block from several allocated ones");
    freeing_one_block_from_several_allocated_test();
    puts("TEST 3: Freeing two blocks from several allocated ones");
    freeing_two_blocks_from_several_allocated_test();
    puts("TEST 4: Memory is over, the new memory region expands the old one");
    memory_ended_and_expanded_test();
    puts("TEST 5: The memory has run out, the old memory region cannot be expanded");
    puts("due to a different allocated address range, the new region is allocated elsewhere");
    memory_ended_and_expanded_elsewhere_test();
}
